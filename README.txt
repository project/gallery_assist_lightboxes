// Copyright 2009 Juan Morejón <jcmc.devel@gmail.com>

Gallery Assist Lightboxes
-------------------------

Links to the supported lightboxes:
      
      Lightbox2   - http://www.huddletogether.com/projects/lightbox2
      Lytebox     - http://www.dolem.com/lytebox
      Thickbox    - http://jquery.com/demo/thickbox
      PrettyPhoto - http://www.no-margin-for-errors.com/projects/prettyPhoto-jquery-lightbox-clone
      fancyBox    - http://fancybox.net
      colorbox    - http://colorpowered.com/colorbox
      shadowbox   - http://www.shadowbox-js.com
      highslide   - http://highslide.com
      interface   - http://interface.eyecon.ro
      slimbbox    - http://www.digitalia.be/software/slimbox2


Install
-------
Copy the lightbox folder to ...your modules/gallery_assist_lightboxes/lightboxes and 
activate the desired lightbox at your_site/admin/settings/gallery_assist/extras 
or by menus Administer >> Site configuration >> Gallery assist >> Extras.

Tips to the each one:

Please see the Info table belonging to this version of Gallery Assist at:

 Administer > Gallery Assist > Exras - fieldset "Lightboxes for Gallery Assist"
 Link: "Get Gallery Assist Lightboxes Info-Table." 
 
or directly at:

 http://www.assist-series.lan/admin/settings/gallery_assist/lightboxes_info 

To get needed version of JQuery to Drupal you must:
JQuery 1.2.6 - Drupal 6.16 provides it by default. No actions needed.
JQuery 1.3.1, 1.3.2 - install JQuery Update v. 6.x-2.x-dev or the latest JQuery Update version containing this version.

If incorrect version of JQuery installed lightboxes will not work!

A comprehensive guide to the installation and to using Gallery Assist with 
Gallery Assist Lightboxes is available as screencasts at:

http://simple.puntolatinoclub.de/screencasts

Incompatibilities
-----------------
* I would be very grateful if you give us feedback in case you get errors or find issues.
I thank you in advance.


Maintainer
----------
The Gallery Assist Lightboxes module was originally developped by:
Juan Carlos Morejon Caraballo

Current maintainer:
Juan Carlos Morejon Caraballo
