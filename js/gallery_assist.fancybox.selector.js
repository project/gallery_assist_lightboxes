// drupal_add_js(drupal_get_path('module', 'gallery_assist_lightboxes')  .'/js/gallery_assist.fancybox.selector.js');

$(document).ready(function() {
  // Settings for fancybox 1.3.1
//  alert(Drupal.settings.lightbox_conf['fancybox']['padding']);
  $(".image-sheet a").fancybox({
    'padding'            : parseInt(Drupal.settings.lightbox_conf['fancybox']['padding']),
    'imageScale'         : parseInt(Drupal.settings.lightbox_conf['fancybox']['imageScale']),
    'zoomOpacity'        : parseFloat(Drupal.settings.lightbox_conf['fancybox']['zoomOpacity']),
//
    'zoomSpeedIn'        : parseInt(Drupal.settings.lightbox_conf['fancybox']['zoomSpeedIn']),
    'zoomSpeedOut'       : parseInt(Drupal.settings.lightbox_conf['fancybox']['zoomSpeedOut']),
    'zoomSpeedChange'    : parseInt(Drupal.settings.lightbox_conf['fancybox']['zoomSpeedChange']),
//
//    'easingIn'           : Drupal.settings.lightbox_conf['fancybox']['easingIn'],
//    'easingOut'          : Drupal.settings.lightbox_conf['fancybox']['easingOut'],
//    'easingChange'       : Drupal.settings.lightbox_conf['fancybox']['easingChange'],
//
//    'frameWidth'         : Drupal.settings.lightbox_conf['fancybox']['frameWidth'],
//    'frameHeight'        : Drupal.settings.lightbox_conf['fancybox']['easingOut'],
//    'overlayShow'        : Drupal.settings.lightbox_conf['fancybox']['overlayShow'],
//
//    'overlayOpacity'     : Drupal.settings.lightbox_conf['fancybox']['overlayOpacity'],
//    'overlayColor'       : Drupal.settings.lightbox_conf['fancybox']['overlayColor'],
//    'enableEscapeButton' : Drupal.settings.lightbox_conf['fancybox']['enableEscapeButton'],
//
//    'showCloseButton'    : Drupal.settings.lightbox_conf['fancybox']['showCloseButton'],
//    'hideOnOverlayClick' : Drupal.settings.lightbox_conf['fancybox']['hideOnOverlayClick'],
//    'hideOnContentClick' : Drupal.settings.lightbox_conf['fancybox']['hideOnContentClick'],
//    'centerOnScroll'     : true
//
//
//
//      padding				:	Drupal.settings.lightbox_conf['fancybox']['padding'], // 10,
//      margin				:	Drupal.settings.lightbox_conf['fancybox']['margin'], // 20,
//      opacity				:	Drupal.settings.lightbox_conf['fancybox']['opacity'], // false,
//      modal				    :	Drupal.settings.lightbox_conf['fancybox']['modal'], // false,
//      cyclic				:	Drupal.settings.lightbox_conf['fancybox']['cyclic'], // false,
//      scrolling			    :	Drupal.settings.lightbox_conf['fancybox']['scrolling'], // 'auto',	// 'auto', 'yes' or 'no'
//
//      width				    :	Drupal.settings.lightbox_conf['fancybox']['width'], // 560,
//      height				:	Drupal.settings.lightbox_conf['fancybox']['height'], // 340,
//
//      autoScale			    :	Drupal.settings.lightbox_conf['fancybox']['autoScale'], // true,
//      autoDimensions		:	Drupal.settings.lightbox_conf['fancybox']['autoDimensions'], // true,
//      centerOnScroll		:	Drupal.settings.lightbox_conf['fancybox']['centerOnScroll'], // false,
//
//      ajax				    :	Drupal.settings.lightbox_conf['fancybox']['ajax'], // {},
//      swf					:	Drupal.settings.lightbox_conf['fancybox']['swf'], // { wmode: 'transparent' },
//
//      hideOnOverlayClick	:	Drupal.settings.lightbox_conf['fancybox']['hideOnOverlayClick'], // true,
//      hideOnContentClick	:	Drupal.settings.lightbox_conf['fancybox']['hideOnContentClick'], // false,
//
//      overlayShow			:	Drupal.settings.lightbox_conf['fancybox']['overlayShow'], // true,
//      overlayOpacity		:	Drupal.settings.lightbox_conf['fancybox']['overlayOpacity'], // 0.3,
//      overlayColor		    :	Drupal.settings.lightbox_conf['fancybox']['overlayColor'], // '#666',
//
//      titleShow			    :	Drupal.settings.lightbox_conf['fancybox']['titleShow'], // true,
//      titlePosition		    :	Drupal.settings.lightbox_conf['fancybox']['titlePosition'], // 'outside',	// 'outside', 'inside' or 'over'
//      titleFormat			:	Drupal.settings.lightbox_conf['fancybox']['titleFormat'], // null,
//
//      transitionIn		    :	Drupal.settings.lightbox_conf['fancybox']['transitionIn'], // 'fade',	// 'elastic', 'fade' or 'none'
//      transitionOut		    :	Drupal.settings.lightbox_conf['fancybox']['transitionOut'], // 'fade',	// 'elastic', 'fade' or 'none'
//
//      speedIn				:	Drupal.settings.lightbox_conf['fancybox']['speedIn'], // 300,
//      speedOut			    :	Drupal.settings.lightbox_conf['fancybox']['speedOut'], // 300,
//
//      changeSpeed			:	Drupal.settings.lightbox_conf['fancybox']['changeSpeed'], // 300,
//      changeFade			:	Drupal.settings.lightbox_conf['fancybox']['changeFade'], // 'fast',
//
//      easingIn			    :	Drupal.settings.lightbox_conf['fancybox']['easingIn'], // 'swing',
//      easingOut			    :	Drupal.settings.lightbox_conf['fancybox']['easingOut'], // 'swing',
//
//      showCloseButton		:	Drupal.settings.lightbox_conf['fancybox']['showCloseButton'], // true,
//      showNavArrows		    :	Drupal.settings.lightbox_conf['fancybox']['showNavArrows'], // true,
//      enableEscapeButton	:	Drupal.settings.lightbox_conf['fancybox']['enableEscapeButton'], // true,
//
//      onStart				:	Drupal.settings.lightbox_conf['fancybox']['onStart'], // null,
//      onCancel			    :	Drupal.settings.lightbox_conf['fancybox']['onCancel'], // null,
//      onComplete			:	Drupal.settings.lightbox_conf['fancybox']['onComplete'], // null,
//      onCleanup			    :	Drupal.settings.lightbox_conf['fancybox']['onCleanup'], // null,
//      onClosed			    :	Drupal.settings.lightbox_conf['fancybox']['onClosed'] // null


//		padding				:	parseInt(Drupal.settings.lightbox_conf['fancybox']['padding']), // 10,
//		margin				:	20,
//		opacity				:	false,
//		modal				:	false,
//		cyclic				:	false,
//		scrolling			:	'auto',	// 'auto', 'yes' or 'no'
//
//		width				:	560,
//		height				:	340,
//
//		autoScale			:	true,
//		autoDimensions		:	true,
//		centerOnScroll		:	false,
//
//		ajax				:	{},
//		swf					:	{ wmode: 'transparent' },
//
//		hideOnOverlayClick	:	true,
//		hideOnContentClick	:	false,
//
//		overlayShow			:	true,
//		overlayOpacity		:	0.3,
//		overlayColor		:	'#666',
//
//		titleShow			:	true,
//		titlePosition		:	'outside',	// 'outside', 'inside' or 'over'
//		titleFormat			:	null,
//
//		transitionIn		:	'fade',	// 'elastic', 'fade' or 'none'
//		transitionOut		:	'fade',	// 'elastic', 'fade' or 'none'
//
//		speedIn				:	300,
//		speedOut			:	300,
//
//		changeSpeed			:	300,
//		changeFade			:	'fast',
//
//		easingIn			:	'swing',
//		easingOut			:	'swing',
//
//		showCloseButton		:	true,
//		showNavArrows		:	true,
//		enableEscapeButton	:	true,
//
//		onStart				:	null,
//		onCancel			:	null,
//		onComplete			:	null,
//		onCleanup			:	null,
//		onClosed			:	null
	});

});

//	$.fn.fancybox.defaults = {
//		padding				:	10,
//		margin				:	20,
//		opacity				:	false,
//		modal				:	false,
//		cyclic				:	false,
//		scrolling			:	'auto',	// 'auto', 'yes' or 'no'
//
//		width				:	560,
//		height				:	340,
//
//		autoScale			:	true,
//		autoDimensions		:	true,
//		centerOnScroll		:	false,
//
//		ajax				:	{},
//		swf					:	{ wmode: 'transparent' },
//
//		hideOnOverlayClick	:	true,
//		hideOnContentClick	:	false,
//
//		overlayShow			:	true,
//		overlayOpacity		:	0.3,
//		overlayColor		:	'#666',
//
//		titleShow			:	true,
//		titlePosition		:	'outside',	// 'outside', 'inside' or 'over'
//		titleFormat			:	null,
//
//		transitionIn		:	'fade',	// 'elastic', 'fade' or 'none'
//		transitionOut		:	'fade',	// 'elastic', 'fade' or 'none'
//
//		speedIn				:	300,
//		speedOut			:	300,
//
//		changeSpeed			:	300,
//		changeFade			:	'fast',
//
//		easingIn			:	'swing',
//		easingOut			:	'swing',
//
//		showCloseButton		:	true,
//		showNavArrows		:	true,
//		enableEscapeButton	:	true,
//
//		onStart				:	null,
//		onCancel			:	null,
//		onComplete			:	null,
//		onCleanup			:	null,
//		onClosed			:	null
