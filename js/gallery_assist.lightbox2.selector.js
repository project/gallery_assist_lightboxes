// drupal_add_js(drupal_get_path('module', 'gallery_assist_lightboxes')  .'/js/gallery_assist.lightbox2.selector.js');

// Initialize the lightbox.
Drupal.behaviors.initLightbox4ga = function (context) {
  $('.gallery-container a.prepared4lb2:not(.lightbox-processed)', context).addClass('lightbox-processed').each(function() {
    $('.lightboxAutoModal').triggerHandler('click');
    return false; // Break the each loop.
  });
};
