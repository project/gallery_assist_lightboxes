// drupal_add_js(drupal_get_path('module', 'gallery_assist_lightboxes')  .'/js/gallery_assist.prettyPhoto.selector.js');

$(document).ready(function(){

  var myAnimationSpeed = Drupal.settings.lightbox_conf['prettyPhoto']['animation_speed'];
  var mySlideshow = Drupal.settings.lightbox_conf['prettyPhoto']['slideshow'];
  var myAutoplaySlideshow = Drupal.settings.lightbox_conf['prettyPhoto']['autoplay_slideshow'];
  var myOpacity = Drupal.settings.lightbox_conf['prettyPhoto']['opacity'];
  var myShowTitle = Drupal.settings.lightbox_conf['prettyPhoto']['show_title'];
  var myAllowResize = Drupal.settings.lightbox_conf['prettyPhoto']['allow_resize'];
  var myDefaultWidth = Drupal.settings.lightbox_conf['prettyPhoto']['default_width'];
  var myDefaultHeight = Drupal.settings.lightbox_conf['prettyPhoto']['default_height'];
  var myCounterSeparatorLabel = Drupal.settings.lightbox_conf['prettyPhoto']['counter_separator_label'];
  var myTheme = Drupal.settings.lightbox_conf['prettyPhoto']['theme'];
  var myModal = Drupal.settings.lightbox_conf['prettyPhoto']['modal'];
  var myOverlayGallery = Drupal.settings.lightbox_conf['prettyPhoto']['overlay_gallery'];

  if (myAnimationSpeed.length == 0) {
    myAnimationSpeed = 'normal';
  }
  if (mySlideshow.length == 0) {
    mySlideshow = 5000;
  } else {
    mySlideshow = parseInt(mySlideshow);
  }
  if (myAutoplaySlideshow.length == 0) {
    myAutoplaySlideshow = false;
  } else {
    myAutoplaySlideshow = parseInt(myAutoplaySlideshow);
  }
  if (myOpacity.length == 0) {
    myOpacity = 0.9;
  }
  if (myShowTitle.length == 0) {
    myShowTitle = false;
  } else {
    myShowTitle = parseInt(myShowTitle);
  }
  if (myAllowResize.length == 0) {
    myAllowResize = true;
  } else {
    myAllowResize = parseInt(myAllowResize);
  }
  if (myDefaultWidth.length == 0) {
    myDefaultWidth = 500;
  } else {
    myDefaultWidth = parseInt(myDefaultWidth);
  }
  if (myDefaultHeight.length == 0) {
    myDefaultHeight = 344;
  } else {
    myDefaultHeight = parseInt(myDefaultHeight);
  }
  if (myCounterSeparatorLabel.length == 0) {
    myCounterSeparatorLabel = '/';
  }
  if (myTheme.length == 0) {
    myTheme = 'facebook';
  }
  if (myModal.length == 0) {
    myModal = true;
  } else {
    myModal = parseInt(myModal);
  }
  if (myOverlayGallery.length == 0) {
    myOverlayGallery = true;
  } else {
    myOverlayGallery = parseInt(myOverlayGallery);
  }


  $("a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed: myAnimationSpeed, /* fast/slow/normal */
      slideshow: mySlideshow, /* false OR interval time in ms */
      autoplay_slideshow: myAutoplaySlideshow, /* true/false */
      opacity: myOpacity, /* Value between 0 and 1 */
      show_title: myShowTitle, /* true/false */
      allow_resize: myAllowResize, /* Resize the photos bigger than viewport. true/false */
      default_width: myDefaultWidth,
      default_height: myDefaultHeight,
      counter_separator_label: myCounterSeparatorLabel, /* The separator for the gallery counter 1 "of" 2 */
      theme: myTheme, /* light_rounded / dark_rounded / light_square / dark_square / facebook */
      modal: myModal, /* If set to true, only the close button will close the window */
      overlay_gallery: myOverlayGallery, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
  });
});
